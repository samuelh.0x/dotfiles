# Complection options
unsetopt AUTO_REMOVE_SLASH  # Do not remove trailing slashes
setopt LIST_PACKED          # Try to make the complection lis smaller by printing matches in columns of different width
setopt LIST_ROWS_FIRST
setopt ALWAYS_TO_END        # Move the cursor to the end of the completed word when a completion is performed
setopt COMPLETE_IN_WORD     # Perform completion from within a word
setopt GLOB_COMPLETE        # When completing a pattern cycle through matches instead of expanding


autoload compinit && compinit -d "${XDG_CACHE_HOME}/zsh/compdump"
zmodload -i zsh/complist

# Use caching for completions
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path "${XDG_CACHE_HOME}/zsh/compcache"

# Use dircolors for completion
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS} 'ma=30;47'

# Use menu completion
zstyle ':completion:*' menu select

# Case-insensitivie, partial and substring completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Formatting
zstyle ':completion:*' verbose yes
zstyle ':completion:*' group-name ''
# zstyle ':completion:*' list-separator '#'
zstyle ':completion:*' list-prompt '%F{white}Completions: list at %p%f'
zstyle ':completion:*' select-prompt '%F{white}Completions: selection at %p%f'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:corrections' format ' %F{blue}Completing: %f%d %F{red}(Errors: %e)%f'
zstyle ':completion:*:descriptions' format "%F{blue}Completing: %f%d"
zstyle ':completion:*:warnings' format "%F{red}No matches found%f"

# Enable approximate completion
zstyle ':completion:*' completer _complete _match _approximate

# Set number of errors relative to word length
zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3)) numeric)'

# Dont complete internal functions
zstyle ':completion:*:functions' ignored-patterns '_*'

# Man pages
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections true
