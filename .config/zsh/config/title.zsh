# Run before a prompt is displayed
set_title_precmd() {
    print -Pn "\e]2;%30<…<%~%<<\a"
}

# Run before a command is executed
set_title_preexec() {
    print -Pn "\e]2;${(q)2} (%30<…<%~%<<)\a"
}

# Hook the functions to set the terminal title
if [[ "$TERM" == (xterm*|rxvt*|tmux*) ]]; then
    add-zsh-hook precmd set_title_precmd
    add-zsh-hook preexec set_title_preexec
fi
