# Handle aliases as arguments
alias sudo='sudo '
alias nohup='nohup '
# Shortcuts
alias c='clear'
alias e='exit'
alias g='git'
alias h='history'
alias j='jobs'
alias o='xdg-open'
alias q='cd ~ && clear'
alias s='sudo '
alias t='tmux'

# Use tmux config under XDG_CONFIG_HOME
alias tmux='tmux -f ~/.config/tmux/tmux.conf'

grep_options=('--color=auto')
ls_options=('--color=auto --group-directories-first')

alias egrep="fgrep ${grep_options[@]}"
alias fgrep="fgrep ${grep_options[@]}"
alias grep="grep ${grep_options[@]}"

alias ll="ls -l ${ls_options[@]}"
alias la="ls -lA ${ls_options[@]}"
alias lh="ls -lAh ${ls_options[@]}"
alias ls="ls ${ls_options[@]}"

alias mkexec='chmod +x'

# Copy and move files with rsync
alias rsc='rsync -avz --info=progress2'
alias rsm='rsync -avz --info=progress2 --remove-source-files'

cdtemp() { cd "$(mktemp -d)" }
mkcd() { mkdir -p "$1" && cd "$1" }

# Network tools
alias lip='ip --color address show'
alias exip='dig +short myip.opendns.com @resolver1.opendns.com'

# Copy to the system clipboard
alias xcopy='xclip -selection clipboard'

# Paste from the system clipboard
alias xpaste='xclip -o -selection clipboard'

# Dotfiles management
alias dots="git --git-dir=${HOME}/.dots --work-tree=${HOME}"

# Systemd
alias jc='journalctl'
alias jcu='journalctl --user'
alias sc='sudo systemctl'
alias scs='systemctl status'
alias scu='systemctl --user'
alias scus='systemctl --user status'

# Pacman
alias pac='pacman'
alias paci='pacman -Si'
alias pacq='pacman -Ql'
alias pacqo='pacman -Qo'
alias pacr='sudo pacman -Rs'
alias pacs='sudo pacman -S'
alias pacss='pacman -Ss'
alias pacu='sudo pacman -Syu'
