HISTFILE="${XDG_DATA_HOME}/zsh/history"

# Number of history entries to save
HISTSIZE=10000
SAVEHIST=10000

# History options
setopt EXTENDED_HISTORY         # Add timestamps to history entries
setopt HIST_IGNORE_ALL_DUPS     # Dont save duplicate entries. Older entry gets removed
setopt HIST_VERIFY              # Dont execute history entries directly but expand into the buffer
setopt INC_APPEND_HISTORY       # Append entries as soon as they are entered and share entries between sessions
setopt SHARE_HISTORY            # Share history between sessions. New entries are imported
