# Options for changing Directories
setopt AUTO_CD              # If a command name is a directory, cd into it
setopt AUTO_PUSHD           # Make cd push the old directory onto the dirstack
setopt PUSHD_IGNORE_DUPS    # Dont push duplicates onto the dirstack

# Prompt options
setopt TRANSIENT_RPROMPT    # Remove the right prompt hen accepting a command line
setopt PROMPT_SUBST         # Perform parameter expansion on the prompt string

autoload -U add-zsh-hook
autoload -U vcs_info

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*' formats ' %F{white} %b%m%f'
zstyle ':vcs_info:git*' actionformats ' %b|%a'
zstyle ':vcs_info:git*+set-message:*' hooks git-dirty

# Git dirty indicator
function +vi-git-dirty() {
    if [[ -n "$(git status --porcelain --ignore-submodules)" ]]; then
        hook_com[misc]='[*]'
    fi
}

# Redraw the prompt when the terminal is resized
TRAPWINCH() {
  zle &&  zle -R
}

# Update the vcs info and store it in psvar[2]
prompt_precmd() {
    psvar=()
    vcs_info
    [[ -n "${vcs_info_msg_0_}" ]] \
        && psvar[2]="${vcs_info_msg_0_}"
}

# Update the prompt when the keymap (VI mode) changes
zle-keymap-select zle-line-init () {
  prompt_mode_indicator
  zle reset-prompt
  zle -R
}
zle -N zle-keymap-select
zle -N zle-line-init

# Set the prompt character depending on the keymap
#   and store it in psvar[1]
prompt_mode_indicator() {
    [[ "$KEYMAP" == vicmd ]] \
        && psvar[1]='❮' \
        || psvar[1]='❯'
}

# Add the username or a root indicator to the prompt if not on the default UID 
prompt_username() {
    [[ $UID -ne 1000 ]] \
        && echo '%(#.%F{red}#.%F{white}(%n%)) '
}

PROMPT="\$(prompt_username)%(1j.%F{blue}[%j]%f .)%F{blue}%-40<…<%~%<<%f\$psvar[2] %(?.%F{blue}.%F{red})\$psvar[1]%f "
RPROMPT="\${SSH_CONNECTION:+(%m)}"

add-zsh-hook precmd prompt_precmd
