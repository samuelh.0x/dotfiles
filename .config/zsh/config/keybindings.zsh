setopt INTERACTIVE_COMMENTS     # Allow comments in interactive shells

# Enable VI mode
bindkey -v

bindkey '^P' up-history
bindkey '^N' down-history

# No delay when changing modes
export KEYTIMEOUT=1
