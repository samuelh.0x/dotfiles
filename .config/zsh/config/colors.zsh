autoload colors && colors

# Load dircolors
if [[ -f "${ZDOTDIR}/dircolors" ]] && \
        command -v dircolors &> /dev/null; then
    eval "$(dircolors ${ZDOTDIR}/dircolors)"
fi

# Colored man pages
export LESS_TERMCAP_md=$'\e[0;34m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[0;37m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[0;32m'
