# Language preferences
export LANG='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'

# Less options
#   * F: Exit if the file can be displayed in one screen
#   * X: Disable termcap initialization
#   * R: Display ANSI escape sequences
#   * M: Display more information in the prompt
#   * J: Display matches in a status column
#   * i: Case insensitive search
export LESS='-FXRMJi'

# Default editor
export EDITOR='nvim'

# Include local binaries in the path
[[ -d "${HOME}/.local/bin" ]] && \
    path=("${HOME}/.local/bin" $path)

# Include local functions in function-path
fpath+=("${ZDOTDIR}/functions")


# Make some progres respect XDG base directory spec
# References:
#   * https://wiki.archlinux.org/index.php/XDG_Base_Directory_support
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="${XDG_CONFIG_HOME}/java"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export GNUPGHOME="${XDG_CONFIG_HOME}/gnupg"
export ICEAUTHORITY="${XDG_CACHE_HOME}/ICEauthority"
export IPYTHONDIR="${XDG_DATA_HOME}/ipython"
export LESSHISTFILE="${XDG_CACHE_HOME}/less/history"
export TMUX_TMPDIR="${XDG_RUNTIME_DIR}"
export WINEPREFIX="${XDG_DATA_HOME}/wineprefixes/default"
export PARALLEL_HOME="${XDG_DATA_HOME}/parallel"
export XINITRC="${XDG_CONFIG_HOME}/X11/initrc"
#export XAUTHORITY="${XDG_RUNTIME_DIR}/Xauthority"
