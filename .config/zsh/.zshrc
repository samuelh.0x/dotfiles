# Exit when not running interactively
[[ $- != *i* ]] && return

# ZSH data directories
[[ -d "${XDG_DATA_HOME}/zsh" ]] || mkdir -p "${XDG_DATA_HOME}/zsh"
[[ -d "${XDG_CACHE_HOME}/zsh" ]] || mkdir -p "${XDG_CONFIG_HOME}/zsh"

# Load local configuration files
for file in ${ZDOTDIR}/config/*.zsh; do
    source "$file"
done

# Check if a plugin exists and load it
load_plugin() {
    [[ -f "${ZDOTDIR}/plugins/${1}.zsh" ]] \
        && source "${ZDOTDIR}/plugins/${1}.zsh"
    return $?
}

# Syntax highlighting for the shell
#   * https://github.com/zsh-users/zsh-syntax-highlighting
if load_plugin "zsh-syntax-highlighting/zsh-syntax-highlighting"; then
    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
fi

# Additional completions
#   * https://github.com/zsh-users/zsh-completions
load_plugin "zsh-completions/zsh-completions.plugin"

# Suggest commands as you type based on the command history
#   * https://github.com/zsh-users/zsh-autosuggestions
load_plugin "zsh-autosuggestions/zsh-autosuggestions"

# Cycle through matches from the command history
#   * https://github.com/zsh-users/zsh-history-substring-search
if load_plugin "zsh-history-substring-search/zsh-history-substring-search"; then
    #bindkey '^[[A' history-substring-search-up
    #bindkey '^[[B' history-substring-search-down
    bindkey '^P' history-substring-search-up
    bindkey '^N' history-substring-search-down
    # bindkey -M vicmd 'k' history-substring-search-up
    # bindkey -M vicmd 'j' history-substring-search-down
    HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bg=green,fg=black'
    HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='bg=red,fg=black'
fi
