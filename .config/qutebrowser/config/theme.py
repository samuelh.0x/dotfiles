c.tabs.padding = {'bottom': 5, 'left': 5, 'right': 5, 'top': 5}

c.fonts.monospace = 'Monospace'
c.fonts.completion.category = '12pt monospace'
c.fonts.completion.entry = '12pt monospace'
c.fonts.debug_console = '12pt monospace'
c.fonts.downloads = '12pt monospace'
c.fonts.hints = '12pt monospace'
c.fonts.keyhint = '12pt monospace'
c.fonts.messages.error = '12pt monospace'
c.fonts.messages.info = '12pt monospace'
c.fonts.messages.warning = '12pt monospace'
c.fonts.prompts = '12pt monospace'
c.fonts.statusbar = '12pt monospace'
c.fonts.tabs =  '12pt monospace'

gruvbox = {
    'background': '#282828',
    'dimbackground': '#32302f',
    'foreground': '#ebdbb2',

    'lightblack': '#504945',
    'darkgray': '#928374',
    'lightgray': '#a89984',

    'lightred': '#fb4934',
    'lightgreen': '#b8bb26',
    'lightyellow': '#fabd2f',
    'lightblue': '#83a598',
    'lightpurple': '#d3869b',
    'lightaqua': '#8ec07c'
}

## Background color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.bg = gruvbox['background']

## Bottom border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.bottom = gruvbox['background']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.category.border.top = gruvbox['background']

## Foreground color of completion widget category headers.
## Type: QtColor
c.colors.completion.category.fg = gruvbox['foreground']

## Background color of the completion widget for even rows.
## Type: QssColor
c.colors.completion.even.bg = gruvbox['dimbackground']

## Text color of the completion widget.
## Type: QtColor
c.colors.completion.fg = gruvbox['foreground']

## Background color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.bg = gruvbox['lightblack']

## Bottom border color of the selected completion item.
## Type: QssColor
c.colors.completion.item.selected.border.bottom = gruvbox['lightblack']

## Top border color of the completion widget category headers.
## Type: QssColor
c.colors.completion.item.selected.border.top = gruvbox['lightblack']

## Foreground color of the selected completion item.
## Type: QtColor
c.colors.completion.item.selected.fg = gruvbox['foreground']

## Foreground color of the matched text in the completion.
## Type: QssColor
c.colors.completion.match.fg = gruvbox['lightaqua']

## Background color of the completion widget for odd rows.
## Type: QssColor
c.colors.completion.odd.bg = gruvbox['dimbackground']

## Color of the scrollbar in completion view
## Type: QssColor
c.colors.completion.scrollbar.bg = gruvbox['dimbackground']

## Color of the scrollbar handle in completion view.
## Type: QssColor
c.colors.completion.scrollbar.fg = gruvbox['lightblack']

## Background color for the download bar.
## Type: QssColor
c.colors.downloads.bar.bg = gruvbox['background']

## Background color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.bg = gruvbox['background']

## Foreground color for downloads with errors.
## Type: QtColor
c.colors.downloads.error.fg = gruvbox['lightred']

## Color gradient start for download backgrounds.
## Type: QtColor
# c.colors.downloads.start.bg = '#0000aa'

## Color gradient start for download text.
## Type: QtColor
c.colors.downloads.start.fg = gruvbox['foreground']

## Color gradient stop for download backgrounds.
## Type: QtColor
# c.colors.downloads.stop.bg = '#00aa00'

## Color gradient end for download text.
## Type: QtColor
# c.colors.downloads.stop.fg = gruvbox['foreground']

## Color gradient interpolation system for download backgrounds.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
# c.colors.downloads.system.bg = 'rgb'

## Color gradient interpolation system for download text.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
# c.colors.downloads.system.fg = 'rgb'

## Background color for hints. Note that you can use a `rgba(...)` value
## for transparency.
## Type: QssColor
c.colors.hints.bg = gruvbox['background']

c.hints.border = '4px solid ' + gruvbox['background']

## Font color for hints.
## Type: QssColor
c.colors.hints.fg = gruvbox['lightaqua']

## Font color for the matched part of hints.
## Type: QssColor
c.colors.hints.match.fg = gruvbox['foreground']

## Background color of the keyhint widget.
## Type: QssColor
c.colors.keyhint.bg = gruvbox['background']

## Text color for the keyhint widget.
## Type: QssColor
c.colors.keyhint.fg = gruvbox['foreground']

## Highlight color for keys to complete the current keychain.
## Type: QssColor
c.colors.keyhint.suffix.fg = gruvbox['lightaqua']

## Background color of an error message.
## Type: QssColor
c.colors.messages.error.bg = gruvbox['background']

## Border color of an error message.
## Type: QssColor
c.colors.messages.error.border = gruvbox['background']

## Foreground color of an error message.
## Type: QssColor
c.colors.messages.error.fg = gruvbox['lightred']

## Background color of an info message.
## Type: QssColor
c.colors.messages.info.bg = gruvbox['background']

## Border color of an info message.
## Type: QssColor
c.colors.messages.info.border = gruvbox['background']

## Foreground color an info message.
## Type: QssColor
c.colors.messages.info.fg = gruvbox['foreground']

## Background color of a warning message.
## Type: QssColor
c.colors.messages.warning.bg = gruvbox['background']

## Border color of a warning message.
## Type: QssColor
c.colors.messages.warning.border = gruvbox['background']

## Foreground color a warning message.
## Type: QssColor
c.colors.messages.warning.fg = gruvbox['lightyellow']

## Background color for prompts.
## Type: QssColor
c.colors.prompts.bg = gruvbox['dimbackground']

## Border used around UI elements in prompts.
## Type: String
c.colors.prompts.border = '1px solid ' + gruvbox['foreground']

## Foreground color for prompts.
## Type: QssColor
c.colors.prompts.fg = gruvbox['foreground']

## Background color for the selected item in filename prompts.
## Type: QssColor
c.colors.prompts.selected.bg = gruvbox['foreground']

## Background color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.bg = gruvbox['background']

## Foreground color of the statusbar in caret mode.
## Type: QssColor
c.colors.statusbar.caret.fg = gruvbox['lightaqua']

## Background color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.bg = gruvbox['dimbackground']

## Foreground color of the statusbar in caret mode with a selection.
## Type: QssColor
c.colors.statusbar.caret.selection.fg = gruvbox['lightaqua']

## Background color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.bg = gruvbox['background']

## Foreground color of the statusbar in command mode.
## Type: QssColor
c.colors.statusbar.command.fg = gruvbox['foreground']

## Background color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.bg = gruvbox['foreground']

## Foreground color of the statusbar in private browsing + command mode.
## Type: QssColor
c.colors.statusbar.command.private.fg = gruvbox['foreground']

## Background color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.bg = gruvbox['background']

## Foreground color of the statusbar in insert mode.
## Type: QssColor
c.colors.statusbar.insert.fg = gruvbox['lightblue']

## Background color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.bg = gruvbox['background']

## Foreground color of the statusbar.
## Type: QssColor
c.colors.statusbar.normal.fg = gruvbox['foreground']

## Background color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.bg = gruvbox['lightpurple']

## Foreground color of the statusbar in passthrough mode.
## Type: QssColor
c.colors.statusbar.passthrough.fg = gruvbox['foreground']

## Background color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.bg = gruvbox['foreground']

## Foreground color of the statusbar in private browsing mode.
## Type: QssColor
c.colors.statusbar.private.fg = gruvbox['foreground']

## Background color of the progress bar.
## Type: QssColor
c.colors.statusbar.progress.bg = gruvbox['foreground']

## Foreground color of the URL in the statusbar on error.
## Type: QssColor
c.colors.statusbar.url.error.fg = gruvbox['lightred']

## Default foreground color of the URL in the statusbar.
## Type: QssColor
c.colors.statusbar.url.fg = gruvbox['foreground']

## Foreground color of the URL in the statusbar for hovelightred links.
## Type: QssColor
c.colors.statusbar.url.hover.fg = gruvbox['foreground']

## Foreground color of the URL in the statusbar on successful load
## (http).
## Type: QssColor
c.colors.statusbar.url.success.http.fg = gruvbox['foreground']

## Foreground color of the URL in the statusbar on successful load
## (https).
## Type: QssColor
c.colors.statusbar.url.success.https.fg = gruvbox['foreground']

## Foreground color of the URL in the statusbar when there's a warning.
## Type: QssColor
c.colors.statusbar.url.warn.fg = gruvbox['lightyellow']

## Background color of the tab bar.
## Type: QtColor
# c.colors.tabs.bar.bg = '#555555'

## Background color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.bg = gruvbox['dimbackground']

## Foreground color of unselected even tabs.
## Type: QtColor
c.colors.tabs.even.fg = gruvbox['darkgray']

## Color for the tab indicator on errors.
## Type: QtColor
c.colors.tabs.indicator.error = gruvbox['lightred']

## Color gradient start for the tab indicator.
## Type: QtColor
c.colors.tabs.indicator.start = gruvbox['lightblue']

## Color gradient end for the tab indicator.
## Type: QtColor
c.colors.tabs.indicator.stop = gruvbox['lightaqua']

## Color gradient interpolation system for the tab indicator.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
# c.colors.tabs.indicator.system = 'rgb'

## Background color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.bg = gruvbox['dimbackground']

## Foreground color of unselected odd tabs.
## Type: QtColor
c.colors.tabs.odd.fg = gruvbox['lightgray']

## Background color of selected even tabs.
## Type: QtColor
c.colors.tabs.selected.even.bg = gruvbox['background']

## Foreground color of selected even tabs.
## Type: QtColor
c.colors.tabs.selected.even.fg = gruvbox['foreground']

## Background color of selected odd tabs.
## Type: QtColor
c.colors.tabs.selected.odd.bg = gruvbox['background']

## Foreground color of selected odd tabs.
## Type: QtColor
c.colors.tabs.selected.odd.fg = gruvbox['foreground']

## Background color for webpages if unset (or empty to use the theme's
## color)
## Type: QtColor
c.colors.webpage.bg = gruvbox['background']
